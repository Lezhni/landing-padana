$ = jQuery;

$(document).on('ready', function() {

	// init one page scroll
	// 1024 - 130 = 894, where 50 - Windows taskbar, 80 - Chrome taskbar
	if ($(window).height() >= 894) {
		$('main').fullpage({
			verticalCentered: false,
	        controlArrows: false,
	        sectionSelector: '.block',
	        lazyLoading: true
	    });
	}

	// init sliders
	$('.js-about-slider').slick({
		slidesToShow: 1,
		fade: true,
		prevArrow: '<span class="about__arrow about__arrow_prev"></span>',
		nextArrow: '<span class="about__arrow about__arrow_next"></span>'
	});

	$('.js-portfolio-slider').slick({
		slidesToShow: 1,
		fade: true,
		prevArrow: '<span class="portfolio__arrow portfolio__arrow_prev"></span>',
		nextArrow: '<span class="portfolio__arrow portfolio__arrow_next"></span>'
	});

	$('.js-certificates-slider').slick({
		slidesToShow: 1,
		fade: true,
		prevArrow: '<span class="certificates__arrow certificates__arrow_prev"></span>',
		nextArrow: '<span class="certificates__arrow certificates__arrow_next"></span>'
	});

	// phone input mask
	$('input[type="tel"]').mask('+7 (999) 999-99-99');
});

// open popup on click `js-popup` class
$('.js-popup').on('click', function(e) {
	e.preventDefault();

	var target = $(this).attr('href');
	showPopup(target);
});

// faq tabs
$('.js-faq').on('click', function() {
	var target = '#' + $(this).attr('aria-controls');

	$('.faq__question, .faq__answer').removeClass('faq__question_active faq__answer_active');
	$('.faq__answer').attr('aria-hidden', 'true');
	$(this).closest('.faq__question').addClass('faq__question_active');
	$(target).addClass('faq__answer_active').attr('aria-hidden', 'false');
});

// open mobile menu
$('.js-menu').on('click', function() {

	var $panel = $('.mobile-panel');
	$('body').toggleClass('menu-opened');

	if ($panel.attr('aria-hidden') == 'true') {
		$panel.attr('aria-hidden', 'false');
	} else {
		$panel.attr('aria-hidden', 'true');
	}
});

// scroll to blocks
// $('.js-scroll-to').on('click', function(e) {
// 	e.preventDefault();
// 	var target = $(this).attr('href');

// 	if (typeof target !== 'undefined') {
// 		$('html, body').animate({
// 			scrollTop: ($(target).offset().top - 75) + 'px' // 75 - header height
// 		}, 1000);
// 	}
// });
